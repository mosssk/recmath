# RecMath

Monorepo for playing around with recreational math ideas that I think of (or more often see elsewhere).

Everything in the repo, unless explicitly marked otherwise, is licensed under the CC0 1.0 Universal license ("No Rights Reserved"). Read more in [LICENSE](./LICENSE), or online at [creativecommons.org](https://creativecommons.org/public-domain/cc0/).
