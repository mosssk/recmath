## Data

This directory contains static data that is compiled into the common library as embedded resources to be used at runtime.

### primes_1048476.bin

This binary file contains bitflags for all odd numbers less than 1,048,576. The bit index is the `floor(number / 2)` 
(since all even numbers are not prime). The bit is 1 if the number is prime, zero otherwise. Because 2 and 3 map to the
same bit in the data, this will return the correct prime-ness for 2.
