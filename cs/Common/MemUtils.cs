﻿/*
 * Copyright (c) 2024 Sean Moss - Creative Commons Zero 1.0 Universal (CC0)
 * This file is licensed under the Creative Commons Zero 1.0 Universal (CC0) license ("No Rights Reserved"). Additional
 * information can be found in the LICENSE file in this repo, or at <https://creativecommons.org/public-domain/cc0/>.
 */

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics;

namespace RecMath;


/// <summary>
/// Contains additional utility functionality related to memory.
/// </summary>
public static unsafe class MemUtils
{
	// Vector sizes (bytes)
	private const uint VEC128_SIZE = 128 / 8;
	private const uint VEC256_SIZE = 256 / 8;

	
	/// <summary>Zeroes the memory in the span of unmanaged types.</summary>
	/// <param name="span">The span to zero the memory of.</param>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public static void ZeroMemory<T>(Span<T> span)
		where T : unmanaged
	{
		var length = (ulong)span.Length;
		fixed (T* spanPtr = span) {
			var ptr = (byte*)spanPtr;
			
			// Handle Vec256
			if (length >= VEC256_SIZE) {
				var zero = Vector256<byte>.Zero;
				while (length >= VEC256_SIZE) {
					Unsafe.WriteUnaligned(ptr, zero);
					length -= VEC256_SIZE;
					ptr += VEC256_SIZE;
				}
			}
			
			// Handle Vec128
			if (length >= VEC128_SIZE) {
				var zero = Vector128<byte>.Zero;
				while (length >= VEC128_SIZE) {
					Unsafe.WriteUnaligned(ptr, zero);
					length -= VEC128_SIZE;
					ptr += VEC128_SIZE;
				}
			}
			
			// Write uint64, then bytes
			while (length >= sizeof(ulong)) {
				Unsafe.WriteUnaligned(ptr, 0ul);
				length -= sizeof(ulong);
				ptr += sizeof(ulong);
			}
			while (length-- >= 1) *ptr++ = 0;
		}
	}


	/// <summary>Reinterprets the span memory from one unmanaged type to the other.</summary>
	/// <param name="span">The span to reinterpret.</param>
	/// <returns>A span over the same memory, reinterpreted as a new type.</returns>
	public static Span<TTo> FastCast<TFrom, TTo>(this Span<TFrom> span)
		where TFrom : unmanaged
		where TTo : unmanaged 
		=> MemoryMarshal.CreateSpan(ref Unsafe.As<TFrom, TTo>(ref span[0]), span.Length * sizeof(TFrom) / sizeof(TTo));
	
	/// <inheritdoc cref="FastCast{TFrom,TTo}(System.Span{TFrom})"/>
	public static ReadOnlySpan<TTo> FastCast<TFrom, TTo>(this ReadOnlySpan<TFrom> span)
		where TFrom : unmanaged
		where TTo : unmanaged
		=> MemoryMarshal.CreateSpan(ref Unsafe.As<TFrom, TTo>(ref Unsafe.AsRef(in span[0])), span.Length * sizeof(TFrom) / sizeof(TTo));
}
