﻿/*
 * Copyright (c) 2024 Sean Moss - Creative Commons Zero 1.0 Universal (CC0)
 * This file is licensed under the Creative Commons Zero 1.0 Universal (CC0) license ("No Rights Reserved"). Additional
 * information can be found in the LICENSE file in this repo, or at <https://creativecommons.org/public-domain/cc0/>.
 */

using System;
using System.Numerics;
using System.Runtime.CompilerServices;
// ReSharper disable InconsistentNaming

namespace RecMath;


/// <summary>
/// Functionality and operations related to prime numbers.
/// </summary>
public static partial class Primes
{
	// Constants
	private static readonly BigInteger BIG_ONE = BigInteger.One;
	private static readonly BigInteger BIG_TWO = new(2u);
	private static readonly BigInteger BIG_SIX = new(6u);
	
	
	/// <summary>Checks if the given value is prime.</summary>
	/// <remarks>This method is fast for numbers below ~1 million, and slows down ~O(n) for larger primes.</remarks>
	/// <param name="value">The value to check the prime-ness for.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool IsPrime(ulong value) => CheckPrimeConstants(value) ?? CheckPrimeFallback(value, 1 << 10);

	/// <inheritdoc cref="IsPrime(ulong)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool IsPrime(BigInteger value) => value <= UInt64.MaxValue 
		? IsPrime((ulong)value) 
		: CheckPrimeFallback(value, UInt32.MaxValue);
	

	/// <summary>Fallback prime checking method utilizing optimized brute force divisor checks.</summary>
	/// <param name="value">The value to check primality for.</param>
	/// <param name="divisor">The starting divisor.</param>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private static bool CheckPrimeFallback(ulong value, ulong divisor = 6)
	{
		const ulong DIVISOR_LIMIT = UInt32.MaxValue;

		if (divisor >= DIVISOR_LIMIT) return CheckPrimeFallback((BigInteger)value, divisor);

		while (divisor * divisor - 2 * divisor + 1 <= value) {
			if (value % (divisor - 1) == 0 || value % (divisor + 1) == 0) return false;
			divisor += 6;
			if (divisor >= DIVISOR_LIMIT) return CheckPrimeFallback((BigInteger)value, divisor);
		}

		return true;
	}

	/// <inheritdoc cref="CheckPrimeFallback(ulong,ulong)"/>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private static bool CheckPrimeFallback(BigInteger value, BigInteger divisor)
	{
		while (divisor * divisor - BIG_TWO * divisor + BIG_ONE <= value) {
			if (value % (divisor - BIG_ONE) == 0 || value % (divisor + BIG_ONE) == 0) return false;
			divisor += BIG_SIX;
		}
		return true;
	}
}
