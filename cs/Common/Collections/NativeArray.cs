﻿/*
 * Copyright (c) 2024 Sean Moss - Creative Commons Zero 1.0 Universal (CC0)
 * This file is licensed under the Creative Commons Zero 1.0 Universal (CC0) license ("No Rights Reserved"). Additional
 * information can be found in the LICENSE file in this repo, or at <https://creativecommons.org/public-domain/cc0/>.
 */

using System;
using System.Runtime.InteropServices;
using JetBrains.Annotations;

// ReSharper disable InconsistentNaming

namespace RecMath.Collections;


/// <summary>
/// Disposable resource that wraps allocated native memory for an array of <c>unmanaged</c> types. The allocated native
/// memory will always be aligned to the size of <typeparamref name="T"/>.
/// </summary>
/// <remarks>This type is not inherently thread-safe.</remarks>
/// <typeparam name="T">The unmanaged type to contain in the array.</typeparam>
public sealed unsafe class NativeArray<T> : IDisposable
	where T : unmanaged
{
	#region Fields
	/// <summary>The number of elements in the array.</summary>
	public readonly uint Count;
	
	/// <summary>The pointer to the unmanaged data. Will throw if accessed after being disposed.</summary>
	public T* Data => IsValid ? _data : throw InvalidAccess();
	private T* _data;


	/// <summary>A span over the unmanaged data. Will throw if accessed after being disposed.</summary>
	public Span<T> DataSpan => IsValid ? new(_data, (int)Count) : throw InvalidAccess();

	/// <summary>The size of the native memory in the array, in bytes.</summary>
	public ulong DataSize => Count * (ulong)sizeof(T);

	/// <summary>If the native memory is still valid (not released/disposed).</summary>
	public bool IsValid => _data != null;
	#endregion // Fields


	private NativeArray(T* ptr, uint count)
	{
		Count = count;
		_data = ptr;
	}

	~NativeArray() => Dispose();

	/// <summary>Allocates a new native array with uninitialized memory.</summary>
	/// <param name="count">The number of elements in the array.</param>
	/// <param name="zeroed">If the array should have its memory zeroed.</param>
	[MustDisposeResource]
	public static NativeArray<T> Alloc(uint count, bool zeroed = false)
	{
		if (count == 0) throw new ArgumentOutOfRangeException(nameof(count), "Cannot create array of size zero");
		var ptr = NativeMemory.AlignedAlloc(count * (nuint)sizeof(T), (nuint)sizeof(T));
		NativeArray<T> arr = new((T*)ptr, count);
		if (zeroed)
			MemUtils.ZeroMemory(arr.DataSpan);
		return arr;
	}

	//
	public void Dispose()
	{
		if (_data == null) return;
		NativeMemory.AlignedFree(_data);
		_data = null;
		GC.SuppressFinalize(this);
	}
	
	
	/// <summary>Gets the item at the given index.</summary>
	public ref T this [uint index] { get {
		if (index >= Count) throw new IndexOutOfRangeException();
		return ref _data[index];
	} }

	/// <inheritdoc cref="this[uint]"/>
	public ref T this[int index] => ref this[(uint)index];


	// Creates an exception for invalid array access
	private static ObjectDisposedException InvalidAccess() => new("Cannot access NativeArray after disposal");
}
