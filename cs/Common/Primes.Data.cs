﻿/*
 * Copyright (c) 2024 Sean Moss - Creative Commons Zero 1.0 Universal (CC0)
 * This file is licensed under the Creative Commons Zero 1.0 Universal (CC0) license ("No Rights Reserved"). Additional
 * information can be found in the LICENSE file in this repo, or at <https://creativecommons.org/public-domain/cc0/>.
 */

using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using RecMath.Collections;

namespace RecMath;


// [Partial] - Constant data for checking primes
public static unsafe partial class Primes
{
	/// <summary>The upper limit of primes that can be checked with <see cref="CheckPrimeConstants"/>.</summary>
	private const ulong PRIME_CONSTANT_LIMIT = PrimeData.FLAGS_SIZE; // 1 << 20
	
	
	/// <summary>Checks the constant prime data to see if the value is prime.</summary>
	/// <remarks>A boolean if the value is known to be prime or not, or <c>null</c> if not determined.</remarks>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private static bool? CheckPrimeConstants(ulong value)
	{
		// Sanity checks
		if (value == 2) return true;
		if (value <= 1 || (value & 1) == 0 || value % 3 == 0 || value % 5 == 0) return false;
		if (value >= PrimeData.FLAGS_SIZE) return null;
		
		// Check prime flags
		var idx = value / 2;
		var fi = idx / 64;
		var bi = idx % 64;
		return (PrimeData.PrimeFlags[fi] & (1ul << (int)bi)) != 0;
	}
}


// Collections of constant prime data
file static unsafe class PrimeData
{
	/// <summary>The maximum prime checkable using <see cref="PrimeFlags"/>.</summary>
	public const int FLAGS_SIZE = 1048576; // 1 << 20

	/// <summary>Bitflags indicating prime-ness for all odd numbers &lt;= <see cref="FLAGS_SIZE"/>.</summary>
	public static readonly ulong* PrimeFlags;


	//
	static PrimeData()
	{
		// Load prime flags
		using (var flagBin = Assembly.GetExecutingAssembly().GetManifestResourceStream("RecMath.Data.primes_1048576.bin")!) {
			Debug.Assert(flagBin.Length % sizeof(ulong) == 0);
			var buffer = NativeArray<ulong>.Alloc((uint)flagBin.Length / sizeof(ulong));
			AppDomain.CurrentDomain.ProcessExit += (_, _) => buffer.Dispose();
			flagBin.ReadExactly(buffer.DataSpan.FastCast<ulong, byte>());
			PrimeFlags = buffer.Data;
		}
	}
}
