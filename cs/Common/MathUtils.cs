﻿/*
 * Copyright (c) 2024 Sean Moss - Creative Commons Zero 1.0 Universal (CC0)
 * This file is licensed under the Creative Commons Zero 1.0 Universal (CC0) license ("No Rights Reserved"). Additional
 * information can be found in the LICENSE file in this repo, or at <https://creativecommons.org/public-domain/cc0/>.
 */

using System.Runtime.CompilerServices;

namespace RecMath;


/// <summary>
/// Additional general mathematics utilities and functionality.
/// </summary>
public static class MathUtils
{
	/// <summary>Maximum optimization flags for <see cref="MethodImplAttribute"/>.</summary>
	public const MethodImplOptions MAX_OPT =
		MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization;
}
